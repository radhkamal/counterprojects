import React from 'react'

const Instructions = () => {
  return (
    <div className='instructions-wrap'>
      <h2>API Route for Checking Counter Value in Database</h2>
      <ul>
        <li>
          <a href="https://counterbyradh.onrender.com/api/counters/getAll" target='_blank'>
          https://counterbyradh.onrender.com/api/counters/getAll</a>
          </li>
      </ul>
    </div>
  )
}

export default Instructions
