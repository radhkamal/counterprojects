import React from 'react'

const Instructions = () => {
  return (
    <div className='instructions-wrap'>
      <h2>API Route for Checking Counter Value in Database</h2>
      <ul>
        <li>
          <a href="https://us-central1-counter-app-2019c.cloudfunctions.net/app/api/getAll" target='_blank'>
            https://us-central1-counter-app-2019c.cloudfunctions.net/app/api/getAll</a>
          </li>
      </ul>
    </div>
  )
}

export default Instructions
