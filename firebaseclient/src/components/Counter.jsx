import React, { useEffect, useRef, useState } from 'react';
import axios from 'axios';

const Counter = () => {
  const [count, setCount] = useState(null);
  const [name, setName] = useState('');
  const currentCountRef = useRef();

  const retrieveCountFromDb = async () => {
    try {
      const response = await fetch("https://us-central1-counter-app-2019c.cloudfunctions.net/app/api/getAll");
      const data = await response.json();
      const counterValue = data.data;
      setCount(counterValue[0].counterStatus);
      setName(counterValue[0].counterName);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    retrieveCountFromDb();
  }, []);

  useEffect(() => {
    retrieveCountFromDb();
  }, [name])


  const handleDecrementCount = async () => {
    setCount((currentState) => {
      if (currentState > 0) {
        console.log(currentState - 1);
        return currentState - 1;
      } else {
        alert("Counter value must be greater than Zero to decrement!");
        console.log(currentState);
        return currentState;
      }
    });

    if (count > 0) {
      await axios.put("https://us-central1-counter-app-2019c.cloudfunctions.net/app/api/update/1682058146097", {
        name: name,
        count: count - 1,
      });
    }
  }

  const handleIncrementCount = async () => {
    setCount((currentState) => {
      console.log(currentState + 1);
      return currentState + 1;
    });

    await axios.put("https://us-central1-counter-app-2019c.cloudfunctions.net/app/api/update/1682058146097", {
      name: name,
      count: count + 1,
    });
  }

  const handleResetCount = async () => {
    setCount(0);

    await axios.put("https://us-central1-counter-app-2019c.cloudfunctions.net/app/api/update/1682058146097", {
      name: name,
      count: 0,
    });
  }

  return (
    <div className="counter-wrap">
      <h1 className='count' ref={currentCountRef}>{count}</h1>
      <div className="buttons">
        <button className="btn decrement" onClick={handleDecrementCount}>-</button>
        <button className="btn reset" onClick={handleResetCount}>Reset</button>
        <button className="btn increment" onClick={handleIncrementCount}>+</button>
      </div>
    </div>
  )
}

export default Counter
