import Card from "./components/Card";
import Instructions from "./components/Instructions";

function App() {
  return (
    <div className="App">
      <Card />
      <Instructions />
    </div>
  );
}

export default App;
