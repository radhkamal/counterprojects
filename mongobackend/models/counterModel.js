const mongoose = require("mongoose");

const counterSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  count: {
    type: Number,
    required: true,
  }
});

const Counters = mongoose.model("Counters", counterSchema);
module.exports = Counters;