const functions = require("firebase-functions");
const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const express = require("express");
const cors = require("cors");

// main app
const app = express();
app.use(cors({
  credentials: true,
  origin: "*",
  methods: ["GET", "PUT", "POST", "DELETE"],
}));

// main database
const db = admin.firestore();

// routes
app.get("/", (request, response) => {
  return response.status(200).send("Express application is running...");
});

app.post("/api/create", async (request, response) => {
  try {
    await db.collection("countDetails").doc(`/${Date.now()}/`).create({
      id: Date.now(),
      counterName: request.body.name,
      counterStatus: request.body.count,
    });

    return response.status(200).send({
      satus: "Success",
      message: "Successfully created Counter object!",
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      status: "Failed",
      message: error,
    });
  }
});

app.get("/api/get/:id", async (request, response) => {
  try {
    const requestedDoc = db.collection("countDetails").doc(request.params.id);
    const countDetail = await requestedDoc.get();
    const requestResponse = countDetail.data();
    return response.status(200).send({
      status: "Success",
      data: requestResponse,
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      status: "Failed",
      message: error,
    });
  }
});

app.get("/api/getAll", async (request, response) => {
  try {
    const query = db.collection("countDetails");
    const responses = [];

    await query.get().then((data) => {
      const docs = data.docs;

      docs.map((singleDoc) => {
        const selectedDoc = {
          counterName: singleDoc.data().counterName,
          counterStatus: singleDoc.data().counterStatus,
        };

        responses.push(selectedDoc);
      });
      return responses;
    });

    return response.status(200).send({
      status: "Success",
      data: responses,
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      status: "Failed",
      message: error,
    });
  }
});

app.put("/api/update/:id", async (request, response) => {
  try {
    const requestedDoc = db.collection("countDetails").doc(request.params.id);
    await requestedDoc.update({
      counterName: request.body.name,
      counterStatus: request.body.count,
    });
    return response.status(200).send({
      status: "Success",
      message: "Counter successfully updated!",
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      status: "Failed",
      message: error,
    });
  }
});

app.delete("/api/delete/:id", async (request, response) => {
  try {
    const requestedDoc = db.collection("countDetails").doc(request.params.id);
    await requestedDoc.delete();
    return response.status(200).send({
      status: "Success",
      message: "Counter successfully deleted!",
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      status: "Failed",
      message: error,
    });
  }
});

// exports the api to firebase cloud functions
exports.app = functions.https.onRequest(app);
