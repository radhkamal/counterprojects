const express = require("express");
const cors = require("cors");
const User = require("./config");
const app = express();
app.use(express.json());
app.use(cors());

app.get("/", async (request, response) => {
  const snapshot = await User.get();
  const list = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
  response.send(list);
});

app.post("/create", async (request, response) => {
  const data = request.body;
  console.log("Data of Users ", data);
  await User.add({ data });
  response.send({ message: "User added!" });
});

app.post("/update", async (request, response) => {
  const id = request.body.id;
  console.log("Before deleting ID ", request.body);
  delete request.body.id;
  console.log("After deleting ID ", request.body);
  const data = request.body;
  await User.doc(id).update(request.body);
  response.send({ message: "User updated!" });
});

app.post("/delete", async (request, response) => {
  const id = request.body.id;
  await User.doc(id).delete();
  response.send({ message: "User deleted!" });
})

app.listen(4000, () => {
  console.log(`Up and running on port 4000`);
});